const express = require('express');
const router = express.Router();

const Cuenta = require('../models/Cuenta');


// mehods post
router.post('/', async (req,res) => {
    const cuenta = new Cuenta(req.body);
    await cuenta.save();
    res.json({
        status: 'Cuenta guardada'
    });
});
// methods GET

/*
*   Obtener toda la b ase de datos de cuentas
*/ 
router.get('/', async (req,res) => {
    const cuentas = await Cuenta.find();
    res.json(cuentas);

    
});

router.get('/comprovar_usuario/:id', async (req,res) => {
    let parametros = req.params.id.split('@');
    
    await Cuenta.findOne({name: parametros[0],password: parametros[1]},function (err,user) {
        res.json(user);
    });
});
router.get('/obtener_nombre/:id', async (req,res) => {
    await Cuenta.findOne({password: req.params.id},'name',function (err, name) {
        res.json(name);
    });
});

// methods delete
router.delete('/borrar_usuario/:id', async (req,res) => {
    await Cuenta.findByIdAndDelete(req.params.id);
    res.json({
        status: 'cuenta borrada'
    })
});

module.exports = router;