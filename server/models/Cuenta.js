const mongoose = require('mongoose');
const { Schema } = mongoose;

const Cuenta = new Schema({
  name: String,
  password: String,
  permission: Number,
  
  subjects: [Schema.Types.Mixed],

  // Cedula de reistro y actualizacion de datos
  ciclo_escolar: String,
  fecha: String,

  //datos generales
  nia: String,
  nombre: String,
  genero: Boolean,



  

})

module.exports =  mongoose.model('Cuenta', Cuenta);


