const express = require("express");
const morgan = require("morgan");
const mongoose = require("mongoose");

const app = express();


// settings
app.set('port',process.env.PORT || 3000);
mongoose.connect('mongodb://localhost/GestionEscolar',{
    useNewUrlParser: true
})
.then(db => console.log('BD is conected'))
.catch(err => console.log(err));

// midlewares
app.use(morgan('dev'));
app.use(express.json());

// routes
app.use('/api/cuentas', require('./routes/cuentas'));

// static files

app.use(express.static('C:\\Users\\A&M\\Documents\\Gestion Escolar\\gestion_escolar\\dist'));

app.listen(app.get('port'),()=>{
  console.log('Server start on port: '+ app.get('port'));
})
