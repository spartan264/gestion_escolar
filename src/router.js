import Vue from 'vue'
import Router from 'vue-router'
import Principal from './views/Principal.vue'
import PNF from './views/PNF.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [    
    {
      path: '/',
      name: 'principal',
      component: Principal
    },
    {
      path: '*',
      component: PNF
    }
  ]
})
